# Primary worker function
# Used to handle all orchestration tasks for the bot
# This will handle all of the AWS calls and integrations required.
import discord
import asyncio
import random
import emoji
import boto3
import threading

topic='/serverless/pubsub'
channel='coder-bottesting'
token='MzQ0MTYxNTExMDAzOTE0MjQx.DRxXGA.iA1CZdrm6RGCO7sxC_6G8kyGtpk'
access_key='AKIAIZ5RBK7KCVE6CYQA'
secret_access_key='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'

def iot_data(message):
    client = boto3.client('iot-data', region_name='us-east-1', aws_access_key_id=access_key, aws_secret_access_key=secret_access_key)

    # Change topic, qos and payload
    client.publish(
        topic=(topic),
        qos=1,
        payload=(message)
    )

client = discord.Client()
content = {}
@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    # Need to add message stuff here.
    # await client.send_message(message.channel, 'Stone-bot starting up and preparing for stream...')

@client.event
async def on_message(message):
    def is_me(m):
        return m.author == client.user
        # BOMB out this is the bots messages.
    def char_is_emoji(character):
        return character in emoji.UNICODE_EMOJI
    def text_has_emoji(text):
        for character in text:
            if character in emoji.UNICODE_EMOJI:
                return True
        return False
    def channel_check(m):
        return m.channel == channel
    def extract_emojis(str):
        return ''.join(c for c in str if c in emoji.UNICODE_EMOJI)

    msg = await client.wait_for_message(author=message.author)

    if is_me(msg):
        # Bot Message discard.
        print("is_me bombing this message")
        return
    elif channel_check(msg):
        # Bomb out if wrong channel
        print("channel_check is bombing this message")
        print(msg.channel)
        return
    elif text_has_emoji(msg.content):
        print("Emoji Found...")
        print(extract_emojis(msg.content))
        print("Sending to IOT...")

        t = threading.Thread(target=iot_data, args=(msg.content,))
        t.start()

        print("Finished sending to IOT...")
        print("All Done With your Emoji")

        return
    else:
        await client.send_message(client.get_channel(channel), '{} - DEBUG Content'.format(msg.content))
        print("No Emoji in Message")

client.run(token)
