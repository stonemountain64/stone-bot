# Feature Template

- [] What (Describe the feature you would like to see.)
- [] Why (Why should we include this feature?)
- [] How (What impact would this feature have.)
