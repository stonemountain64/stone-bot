### StoneMountain64 Discord Chat/Voice Bot

# This is a place holder for a generic discord chat bot

[![build status](https://gitlab.com/stonemountain64/stone-bot/badges/master/build.svg)](https://gitlab.com/stonemountain64/stone-bot/commits/master)

Make some slight updates here for git hook testing.
docker build --tag=sshtest --build-arg SSH_KEY="$(cat ~/.ssh/path-to-private.key)" .dockerfile

There will be a single docker container that run's as the "bot", using IOT MQTT topics for all communication.  With an s3/lambda web page with live feed.

# Database
- Guild/Server Database (All servers connected)
  - Associated Channel watched/streamed
- Message stream log?

# IOT
- Live channel stream

# Docker Bot - Phase 1
- worker - feed SQS (IOT)
  possibly use https://github.com/aio-libs/aiobotocore for async iot?
